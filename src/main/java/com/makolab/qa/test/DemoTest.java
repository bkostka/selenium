package com.makolab.qa.test;

import com.makolab.qa.page.BudgetCalculatorMainPage;
import com.makolab.qa.page.ConfigurationPage;
import com.makolab.qa.util.BaseTest;
import com.makolab.qa.util.Direction;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class DemoTest extends BaseTest {

    private ConfigurationPage configurationPage;

    @Before
    public void prepare() {
        this.configurationPage = new ConfigurationPage(driver);
    }

    @Test
    public void moveSlidersDemoTest() {
        BudgetCalculatorMainPage budgetCalculatorMainPage = configurationPage.openInModal();

        // move upper left drag and drop
        budgetCalculatorMainPage
                .dragAndDrop(budgetCalculatorMainPage.getSpendMonthlyLeftDragAndDrop(), 4400, Direction.FORWARD);
        budgetCalculatorMainPage
                .dragAndDrop(budgetCalculatorMainPage.getSpendMonthlyLeftDragAndDrop(), 4300, Direction.BACKWARD);

        // move upper right drag and drop
        budgetCalculatorMainPage
                .dragAndDrop(budgetCalculatorMainPage.getSpendMonthlyRightDragAndDrop(), 6000, Direction.BACKWARD);
        budgetCalculatorMainPage
                .dragAndDrop(budgetCalculatorMainPage.getSpendMonthlyRightDragAndDrop(), 8000, Direction.FORWARD);

        // move lower drag and drop
        budgetCalculatorMainPage
                .dragAndDrop(budgetCalculatorMainPage.getDurationDragAndDrop(), 12, Direction.BACKWARD);
        budgetCalculatorMainPage
                .dragAndDrop(budgetCalculatorMainPage.getDurationDragAndDrop(), 48, Direction.FORWARD);

        // sleep to see results
        try {
            TimeUnit.SECONDS.sleep(20);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
