package com.makolab.qa.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageObject {

    protected WebDriverWait waiter;
    protected WebDriver driver;

    public PageObject(WebDriver driver) {
        this.driver = driver;
        waiter = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }
}
