package com.makolab.qa.page;

import com.makolab.qa.util.Direction;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class BudgetCalculatorMainPage extends PageObject {

    @FindBy(xpath = "//*[@id=\"multivalueMonthlyPayment_32_multivalueSlider_slider\"]/div/div[1]/div")
    @Getter
    private WebElement spendMonthlyLeftDragAndDrop;

    @FindBy(xpath = "//*[@id=\"multivalueMonthlyPayment_32_multivalueSlider_slider\"]/div/div[3]/div")
    @Getter
    private WebElement spendMonthlyRightDragAndDrop;

    @FindBy(xpath = "//*[@id=\"multivalueDuration_32_multivalueSlider_slider\"]/div/div/div")
    @Getter
    private WebElement durationDragAndDrop;


    public BudgetCalculatorMainPage(WebDriver driver) {
        super(driver);
    }

    public BudgetCalculatorMainPage dragAndDrop(
            WebElement element, int toValue, Direction direction) {
        int stepPixels = calculatePixelsForStep(element, direction);

        while (getAriaValueTextAsInt(element) < toValue) {
            Actions builder = new Actions(driver);
            Action dragAndDrop =
                    builder.dragAndDropBy(element, stepPixels, 0)
                            .build();
            dragAndDrop.perform();
        }
        return this;
    }

    private int calculatePixelsForStep(WebElement element, Direction direction) {
        Actions builder = new Actions(driver);
        int baseAriaValue = getAriaValueTextAsInt(element);

        int stepPixels = Math.abs(direction.getValue());
        while (baseAriaValue == getAriaValueTextAsInt(element)) {
            Action dragAndDrop =
                    builder.dragAndDropBy(element, stepPixels * direction.getValue(), 0)
                            .build();
            dragAndDrop.perform();
            ++stepPixels;
        }
        return stepPixels;
    }

    private int getAriaValueTextAsInt(WebElement element) {
        return Integer.parseInt(
                element.getAttribute("aria-valuetext")
                        .replaceAll("[^0-9]+", ""));
    }
}
