package com.makolab.qa.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ConfigurationPage extends PageObject {

    @FindBy(className = "btn-warning")
    private WebElement openInBodyButton;

    public ConfigurationPage(WebDriver driver) {
        super(driver);
    }

    public BudgetCalculatorMainPage openInModal() {
        waiter.until(ExpectedConditions.visibilityOf(openInBodyButton)).click();
        return PageFactory.initElements(driver, BudgetCalculatorMainPage.class);
    }
}
