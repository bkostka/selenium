package com.makolab.qa.util;

import com.makolab.qa.factory.DriverFactory;
import org.junit.After;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;

public class BaseTest {
    protected static WebDriver driver;

    @BeforeClass
    public static void setUp() {
        driver = DriverFactory.getInstance();
        driver.get("https://qa-finance.makolab.net/integration/dk-bc/toyota-amd.html");
    }

    @After
    public void cleanUp() {
        driver.quit();
    }
}
