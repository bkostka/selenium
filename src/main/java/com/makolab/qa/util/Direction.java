package com.makolab.qa.util;

import lombok.Getter;

public enum Direction {
    FORWARD(1),
    BACKWARD(-1);

    @Getter
    private int value;

    Direction(int value) {
        this.value = value;
    }
}
